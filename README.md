# SKILL DE VOZ: INSPEÇÃO DRONE #

### Sobre o repositório ###

* Skill de voz Amazon Alexa para controle da inspeção utilizando drone desenvolvida para Echo Dot 
* Versão 0.2

## Instruções para configuração da skill
* Como criar uma skill Amazon Alexa?
* Importar JSON modelo de interação de voz
* Importar código serverless da AWS lambda
* Como testar?

## Como criar uma skill Amazon Alexa?
1. Acesso ao console de desenvolvimento [Amazon Alexa](https://github.com/nestjs/nest)
* Caso não tenha conta, crie uma nesse momento.
2. Em "Skills" selectionar "Create Skill" para abrir o wizard.
![drone inspection skill](/.img/alexa2.jpg)
3. Na tela "Create new Skill" preencher no formulário:
* Skill name:  (ex: minha skill)
* Default language: Portuguese (BR)
* Choose a model to add to your skill": CUSTOM
* Choose a method to host your skill's backend resources: Provision your own
![drone inspection skill](/.img/alexa3.jpg)
![drone inspection skill](/.img/alexa4.jpg)
4. Clicar create skill.
5. Na tela "Choose a template to add to your skill" selecionar "Hello World Skill".
![drone inspection skill](/.img/alexa5.jpg)
6. Clicando no botão "Continue with template" criará a skill.

## Importar JSON modelo de interação de voz
1. Clonar este repositório localmente:
```bash
$ git clone https://(username)@bitbucket.org/ggleme/drone-inspection-skill.git
```
2. Na aba "Build" da skill selecione o "JSON Editor".
![drone inspection skill](/.img/alexa6.jpg)
3. Importe o modelo de interação (pt-BR.json) diretamente no editor.
```bash
drone-inspection-skill/src/skill-package/interactionModels/custom/pt-BR.json
```
4. Clicar em "Save Model" e depois em "Build Model"
5. Por último selecione o item "Invocation" e substitua a frase de invocação que será utilizada para abrir a skill no dispositivo Echo Dot.
* Nota: observar a estrutura da skill do drone. Ela possui poucas customizações: o DroneIntent que utiliza o slot ComandosDrone para definir os comandos de voz disponíveis.
![drone inspection skill](/.img/alexa1.jpg)

## Referenciar função serverless da AWS lambda
**Importante**: para conclusão deste passo é necessário ter feito deploy da função lambda conforme procedimento do [Drone Commands Interpreter](https://bitbucket.org/ggleme/drone-commands-interpreter/src/master/README.md)
1. Clique em "Endpoint" para obter as informações da Skill ID e ARN da função Lambda
![drone inspection skill](/.img/alexa7.jpg)
2. Copie em um editor a "Skill ID" pois ela será necessária para configurar o commands interpreter e autorizar o acesso para execução.
4. Após deploy do "Drone Commands Interpreter" vá até o console da AWS Lambda e copie o ID "AWS Lambda ARN" da função. Este ID deve ser preenchido no campo "Default Region".
5. Clicar em "Save Endpoints" para efetivar as alterações.

## Como testar?
1. Habilite os testes da skill na aba "Test" selecionando a opção "Development".
2. Após habilitado, a skill pode ser testada em qualquer dispositivo compatível com a Amazon Alexa.
